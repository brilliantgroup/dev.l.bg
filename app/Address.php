<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    private $id;


    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['street', 'zip', 'town', 'phone', 'cell', 'country_id', 'user_id'];

    protected $guarded = ['is_default'];

    public function __construct()
    {

    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }
}
