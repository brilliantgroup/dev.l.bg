<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPage extends Model
{
    protected $table = 'admins_pages';

    protected $fillable = [
        'page_route', 'page_name', 'page_description',
        'parent_id',
        'page_priority',
        'page_level',
        'page_icon',
        'is_visible',
        'page_modify',
        'page_refresh',
        'page_hasoptions'
    ];

    public function childs()
    {
        return $this->hasMany('App\AdminPage', 'parent_id', 'id');
    }

    public function permissions()
    {
        return $this->hasMany('App\AdminPagePermission', 'page_id', 'id');
    }

}


//$users = User::whereHas('posts', function($q){
//    $q->where('description','like','Описание-%');
//})->get();

//App\AdminPage::where('id', 1)->with('permissions')->get()
//App\AdminPage::where('id', 1)->whereHas('permissions', function($q){$q->where('role_id','1');$q->where('show','1');})->get();
//App\AdminPage::where([ ['page_level', '>', '0'],['is_visible', '1']])->whereHas('permissions', function($q){$q->where('role_id','1');$q->where('show','1');})->get();