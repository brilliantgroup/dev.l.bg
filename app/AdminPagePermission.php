<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminPagePermission extends Model
{
    protected $table = 'admins_pages_permissions';

    protected $fillable = [
        'page_id', 'role_id', 'show'
    ];

    public function pages()
    {
        return $this->belongsTo('App\AdminPage', 'id', 'page_id');
    }

//    protected $fillable = [
//        'page_route', 'page_name', 'page_description',
//        'parent_id',
//        'page_priority',
//        'page_level',
//        'page_icon',
//        'is_visible',
//        'page_modify',
//        'page_refresh',
//        'page_hasoptions'
//    ];
}
