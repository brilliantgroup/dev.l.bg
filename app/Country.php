<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $fillable = [
        'priority', 'code', 'name',
    ];

    public function address()
    {
        return $this->hasMany('App\Address', 'country_id', 'id');
    }
}
