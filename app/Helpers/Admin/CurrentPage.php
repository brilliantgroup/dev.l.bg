<?php
/**
 * Created by PhpStorm.
 * User: holy
 * Date: 12/16/17
 * Time: 7:14 PM
 */

namespace App\Helpers\Admin;

use App\AdminPage;
use Illuminate\Support\Facades\Route;
use Auth;
use DB;


class CurrentPage
{
    public $page;
    public $userRole;

    public function __construct()
    {
        $this->page = $this->getPage();
        $this->userRole = $this->getUserRole();
        $this->pagePermissions = $this->getPagePermissions();
    }

    private function getPage()
    {
        $route = Route::currentRouteName();
        $pattern = '/get./';
        $replacement = 'admin.';
        $currentRoute = preg_replace($pattern, $replacement, $route);

        return AdminPage::where('admins_pages.page_route', $currentRoute)
            ->get()->first();
    }

    private function getUserRole()
    {
        return Auth::guard('admin')->user()->roles->first()->id;
    }

    private function getPagePermissions()
    {
        return $this->page->permissions->where('role_id', $this->userRole)->first();
    }
}