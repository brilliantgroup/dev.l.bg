<?php

namespace App\Http\Composers;

use App\AdminPage;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
use App\Helpers\Admin\CurrentPage;

class NavigationComposers
{

    public function __construct(CurrentPage $currentPage)
    {
        $this->userRole = $currentPage->userRole;
    }

    public function compose(View $view)
    {

        $menu = $this->getMenuTree();
        $currentPage = $this->getCurrentPage();

        $view->with(compact('menu', 'currentPage'));
    }

    private function getMenuTree()
    {
        $menu = [];

        $_menu = AdminPage::where([
                ['page_level', '>', '0'],
                ['is_visible', '1']]
        )
            ->whereHas('permissions', function($q){
                $q->where('role_id', $this->userRole);
                $q->where('show','1');
            })
            ->orderBy('page_priority', 'asc')
            ->get()->toArray();


        foreach ($_menu ?? [] as $k => $m) {
            if ($m["parent_id"] > 0) {
                $menu[$m["parent_id"]]["childrens"][] = $m;
            } else {
                $menu[$m["id"]] = $m;
            };
        }

        return $menu;
    }

    private function getCurrentPage()
    {
        return AdminPage::where('admins_pages.page_route', Route::currentRouteName())
            ->whereHas('permissions', function($q){
                $q->where('role_id','3');
            })
            ->get()->first();
    }
}