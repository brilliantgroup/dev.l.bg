<?php

namespace App\Http\Composers;

use App\AdminPage;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
use App\Helpers\Admin\CurrentPage;

class PageHeaderComposers
{

    private $pagePermissions;

    public function __construct(CurrentPage $currentPage)
    {
        $this->pagePermissions = $currentPage->pagePermissions;
    }

    public function compose(View $view)
    {
        $currentPage = $this->getCurrentPage();
        $breadcrumbs = $this->getBreadCrumbs();
        $pagePermissions = $this->pagePermissions;

        $view->with(compact('breadcrumbs', 'currentPage', 'pagePermissions'));
    }

    private function getCurrentPage() {
        return AdminPage::where('admins_pages.page_route', Route::currentRouteName())
            ->first();
    }

//        TODO $breadcrumbs
    private function getBreadCrumbs() {
        return AdminPage::where('admins_pages.page_route', Route::currentRouteName())
            ->get();
    }
}