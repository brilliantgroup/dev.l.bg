<?php

namespace App\Http\Controllers\Admin;

use App\AdminRole;
use App\Http\Requests\StoreAdmin;
use DataTables;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.admins');
    }

    public function getData()

    {
        return DataTables::of(Admin::query())
            ->addColumn('action', function ($user) {
                $_action = '
                    <a class="update" alt="Update record" href="javascript:;" rel="' . $user->id . '"><i class="fa fa-pencil"></i></a>
                    <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $user->id . '"><i class="fa fa-trash-o"></i></a>';

                return $_action;
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new Admin();

        $roles = AdminRole::orderBy('display_name')->pluck('display_name', 'id');

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );


        $result["form"] = view('admin.forms.admin', compact('user', 'roles'))->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdmin $request)
    {
        $user = Admin::create($request->all());

        if ($request->roles) {
            $user->roles()->sync($request->roles);
        }

        return Response::json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::find($id);
        $roles = AdminRole::orderBy('display_name')->pluck('display_name', 'id');

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );


        $result["form"] = view('admin.forms.admin', compact('user', 'roles'))->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Admin::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->job_title = $request->job_title;

        if ($request->password) {
            $user->password = $request->password;
        }

        $user->update();

        if ($request->roles) {
            $user->roles()->sync($request->roles);
        }

        return Response::json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $user = Admin::destroy($id);
        return Response::json($user);
    }
}
