<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreAdminPage;
use App\Http\Requests\StoreSitePage;
use DataTables;
use App\AdminPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class AdminPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = AdminPage::where('page_level', '=', 1)->get();

        return view('admin.adminpages', ['pages' => $pages]);
    }

    public function getData()

    {
        $pages = AdminPage::where('page_level', '=', 1)->get();
//        $allPages = Category::pluck('title','id')->all();

        return $result = view('admin.adminpages', ['pages' => $pages])->render();

    }

    public function create()
    {
        $page = new AdminPage();
        $pages = AdminPage::where('page_level', '=', 1)->pluck('page_name', 'id');

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );

        $result["form"] = view('admin.forms.adminpage', compact('page', 'pages'))->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminPage $request)
    {
        $page = new AdminPage();

        $page->page_route = $request->page_route;
        $page->page_name = $request->page_name;
        $page->parent_id = $request->parent_id;
        $page->page_description = $request->page_description;
        $page->page_priority = $request->page_priority;
        $page->page_level = ($request->parent_id>0)?2:1;
        $page->page_icon = $request->page_icon;
        $page->is_visible = $request->is_visible;
        $page->page_modify = $request->page_modify;
        $page->page_refresh = $request->page_refresh;
        $page->page_hasoptions = $request->page_hasoptions;

        $page->save();

        $result = ['redirect'=>'true'];
        return Response::json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = AdminPage::find($id);

        $pages = AdminPage::where('page_level', '=', 1)->pluck('page_name', 'id');

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.adminpage', compact('page', 'pages'))->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAdminPage $request, $id)
    {
        $page = AdminPage::find($id);

        $page->page_route = $request->page_route;
        $page->page_name = $request->page_name;
        $page->parent_id = $request->parent_id;
        $page->page_description = $request->page_description;
        $page->page_priority = $request->page_priority;
        $page->page_level = ($request->parent_id>0)?2:1;
        $page->page_icon = $request->page_icon;
        $page->is_visible = $request->is_visible;
        $page->page_modify = $request->page_modify;
        $page->page_refresh = $request->page_refresh;
        $page->page_hasoptions = $request->page_hasoptions;

//        $page->save();
        $page->update();
        $result = ['redirect'=>'true'];
        return Response::json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AdminPage::destroy($id);

        $result = ['redirect'=>'true'];
        return Response::json($result);
    }
}
