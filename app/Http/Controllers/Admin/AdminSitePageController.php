<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreSitePage;
use DataTables;
use App\SitePage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class AdminSitePageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sitepages');
    }

    public function getData()

    {
        return DataTables::of(SitePage::query())
            ->addColumn('action', function ($sitepage) {
                $_action = '<a class="update" alt="Update record" href="javascript:;" rel="' . $sitepage->id . '"><i class="fa fa-pencil"></i></a>
                 <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $sitepage->id . '"><i class="fa fa-trash-o"></i></a>';

                return $_action;
            })
            ->make(true);
    }

    public function create()
    {
        $page = new SitePage();

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );

        $result["form"] = view('admin.forms.sitepage', ['page' => $page])->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSitePage $request)
    {
        $page = SitePage::create($request->all());

        return Response::json($page);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = SitePage::find($id);

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.sitepage', ['page' => $page])->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSitePage $request, $id)
    {
        $page = SitePage::find($id);
        $page->update($request->all());
        return Response::json($page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = SitePage::destroy($id);
        return Response::json($page);
    }


}
