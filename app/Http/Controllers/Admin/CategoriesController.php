<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\StoreCategory;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class CategoriesController extends Controller
{
    private $category;

    public function __construct(Category $category)
    {
        $this->middleware('auth:admin');
        $this->category = $category;
    }

    public function index()
    {
        return view('admin.categories');
    }

    public function getData()

    {
        return DataTables::of(Category::query())
            ->addColumn('action', function ($category) {
                $_action = '<a class="update" alt="Update record" href="javascript:;" rel="' . $category->id . '"><i class="fa fa-pencil"></i></a>
                 <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $category->id . '"><i class="fa fa-trash-o"></i></a>';

                return $_action;
            })
            ->make(true);
    }

    public function create()
    {
        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );

        $result["form"] = view('admin.forms.category', ['category' => $this->category])->render();
        return $result;
    }

    public function store(StoreCategory $request)
    {
        $category = $this->category->create($request->all());

        return Response::json($category);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = $this->category->find($id);

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.category', ['category' => $category])->render();
        return $result;
    }

    public function update(StoreCategory $request, $id)
    {
        $category = $this->category->find($id);
        $category->update($request->all());

        return Response::json($category);
    }

    public function destroy($id)
    {
        $category = $this->category->destroy($id);
        return Response::json($category);
    }
}
