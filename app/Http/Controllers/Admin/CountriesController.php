<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreCountry;
use DataTables;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Helpers\Admin\CurrentPage;

class CountriesController extends Controller
{
    private $country;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Country $country)
    {
        $this->middleware('auth:admin');
        $this->country = $country;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.countries');
    }

    public function getData(CurrentPage $currentPage)

    {

        $this->pagePermissions = $currentPage->pagePermissions;

        return DataTables::of(
            Country::where('code', '=', 'US')
        ->orWhere('code', '=', 'CA')
        ->where('priority', '=', '1')
        ->orWhere('priority', '=', '3')
//        ->orWhereNull('endAmount')
//        ->where('user_id', Auth::user()->id)
        ->get()
        )
            ->addColumn('action', function ($sitepage) {
                $_action = '';

                if ($this->pagePermissions->edit) {
                    $_action .= '<a class="update" alt="Update record" href="javascript:;" rel="' . $sitepage->id . '"><i class="fa fa-pencil"></i></a>';
                }

                if ($this->pagePermissions->delete) {
                    $_action .= '<a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $sitepage->id . '"><i class="fa fa-trash-o"></i></a>';
                }

                return $_action;
            })
            ->make(true);
    }

    public function create()
    {
//        $country = new Country();

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );

        $result["form"] = view('admin.forms.country', ['country' => $this->country])->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCountry $request)
    {
        $country = $this->country->create($request->all());

        return Response::json($country);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = $this->country->find($id);

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.country', ['country' => $country])->render();



//        return response()->json($result);


//        return $country;
        return $result;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCountry $request, $id)
    {
        $country = $this->country->find($id);
        $country->update($request->all());

        return Response::json($country);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = $this->country->destroy($id);
        return Response::json($country);
    }
}
