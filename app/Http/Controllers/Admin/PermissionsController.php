<?php

namespace App\Http\Controllers\Admin;

use App\AdminRole;
use App\AdminPage;
use App\AdminPagePermission;
use App\Http\Requests\StoreAdmin;
use DataTables;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class PermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.permissions');
    }

    public function getData()

    {
        return DataTables::of(AdminRole::query())
            ->addColumn('action', function ($user) {
                $_action = '
                    <a class="update" alt="Update record" href="javascript:;" rel="' . $user->id . '"><i class="fa fa-pencil"></i></a>';

                return $_action;
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = new AdminRole();

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );


        $result["form"] = view('admin.forms.permission', compact('role'))->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdmin $request)
    {
        $role = AdminRole::create($request->all());

//        if ($request->roles) {
//            $user->roles()->sync($request->roles);
//        }

        return Response::json($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = AdminRole::find($id);
        $pages = AdminPage::all();

        $permission = AdminPagePermission::where(['role_id' => $id])->get();
        $permissions = $permission->mapWithKeys(function ($item) {
            return [$item['page_id'] => $item];
        })->all();

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );


        $result["form"] = view('admin.forms.permission', compact('role', 'pages', 'permissions'))->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = AdminRole::find($id);
        $role->display_name = $request->display_name;
        $role->update();

        AdminPagePermission::where('role_id', $id)->delete();

        if ($request->pages) {
            foreach($request->pages as $k=>$v) {
                $p = new AdminPagePermission();
                $p->page_id = $v;
                $p->role_id = $id;
                $p->show = $request->show[$v]??0;
                $p->options = $request->options[$v]??0;
                $p->add = $request->add[$v]??0;
                $p->view = $request->view[$v]??0;
                $p->edit = $request->edit[$v]??0;
                $p->delete = $request->delete[$v]??0;
                $p->save();
            }
        }
        return Response::json($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $role = AdminRole::destroy($id);
        return Response::json($role);
    }
}
