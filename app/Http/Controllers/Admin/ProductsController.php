<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Requests\StoreProduct;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ProductsController extends Controller
{
    private $product;

    public function __construct(Product $product)
    {
        $this->middleware('auth:admin');
        $this->product = $product;
    }

    public function index()
    {
        return view('admin.products');
    }

    public function getData()

    {
        return DataTables::of(Product::query())
            ->addColumn('action', function ($item) {
                $_action = '<a class="update" alt="Update record" href="javascript:;" rel="' . $item->id . '"><i class="fa fa-pencil"></i></a>
                 <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $item->id . '"><i class="fa fa-trash-o"></i></a>';

$_action = "";
                return $_action;
            })
            ->make(true);
    }

    public function create()
    {
        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );

        $result["form"] = view('admin.forms.product', ['product' => $this->product])->render();
        return $result;
    }

    public function store(StoreProduct $request)
    {
        $product = $this->product->create($request->all());

        return Response::json($product);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = $this->product->find($id);

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.product', ['product' => $product])->render();
        return $result;
    }

    public function update(StoreProduct $request, $id)
    {
        $product = $this->product->find($id);
        $product->update($request->all());

        return Response::json($product);
    }

    public function destroy($id)
    {
        $product = $this->product->destroy($id);
        return Response::json($product);
    }
}
