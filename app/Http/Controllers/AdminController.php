<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;
//use App\AdminRole;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

//        var_dump(auth()->user());
//        var_dump(Auth::user());
//        var_dump($this->auth = $auth);


        $this->middleware('auth:admin');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\App\Helpers\Admin\CurrentPage $currentPage)
    {

//        var_dump($currentPage->page);
//        dd($this->uid = Auth::guard('admin')->user()->id);
//        dd(Auth::guard('admin')->user()->hasRole('super_admin'));
//        dd(Auth::user()->getIdInArray('superadmin'));
//        dd(Admin::getUserName(Auth::user()));

//        $aa = new Admin();
//        $a = $aa->roles()->get();
//        foreach ($a as $role) {
//            var_dump($role);
//            var_dump("12");
//        }

//        var_dump($a);

//        dd(Auth::user());
//        dd(Auth::user()->roles()->users());
//        dd(Auth::user()->roles()->fisrt()->get());
//        dd(Auth::user()->hasRole('superadmin')->role_id);
//        dd(Auth::user()->isEmployee());
//        if(Auth::user()->hasRole('super_admin')) {
            /* тут ваш код */
//        }

//        dd()

        $page = $currentPage->page;

        return view('admin', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $uid = Auth::id();

        $user = Admin::find($uid);
        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );

        $result["form"] = view('admin.forms.profile', ['user' => $user])->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $uid = Auth::id();

        $user = Admin::find($uid);
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        $user->save();
        return 1;
    }


    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route( 'admin.login' ));
    }

}
