<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use DataTables;
use App\User;
use App\Country;
use App\Address;
use Illuminate\Http\Request;

use App\Jobs\SendNotificationEmailForUserUpdateProfile;

use Illuminate\Support\Facades\Response;


class UserController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth:admin');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users');
    }

    public function getData()

    {
        return DataTables::of(User::query())
            ->addColumn('action', function ($user) {
                $_action = '
                    <a class="update" alt="Update record" href="javascript:;" rel="' . $user->id . '"><i class="fa fa-pencil"></i></a>
                    <a class="delete-row delete" alt="Remove record" href="javascript:;" rel="' . $user->id . '"><i class="fa fa-trash-o"></i></a>';

                return $_action;
            })
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;

        $countries = Country::pluck('name', 'id');
        $addresses = [new Address()];
        $code = $code = $this->generateRandom();

        $result["title"] = "Add";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Add</button>'
        );


        $result["form"] = view('admin.forms.user', compact('user', 'countries', 'addresses', 'code'))->render();
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $user = User::create($request->all());

        if (count($request->address)) {

                $user->addresses()->createMany($request->address);

//            foreach ($request->address as $key => $addr) {
//                $address = new Address([
//                    'user_id' => $user->id,
//                    'country_id' => $addr["country_id"]??'',
//                    'street' => $addr["street"] ?? '',
//                    'zip' => $addr["zip"] ?? '',
//                    'town' => $addr["town"] ?? '',
//                    'phone' => $addr["phone"] ?? '',
//                    'cell' => $addr["cell"] ?? ''
//                ]);
//
//                $user->addresses()->save($address);
//            }
        }

        return Response::json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        $countries = Country::pluck('name', 'id');
        if ($user->addresses()->get()->isEmpty()) {
            $addresses = [new Address()];
            $code = $this->generateRandom();
        } else {
            $addresses = $user->addresses()->get();
            $code = false;
        }

//        var_dump($code);

        $result["title"] = "Update";
        $result["buttons"] = array(
            '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
            '<button type="button" class="btn btn-primary submit" id="btn-save">Update</button>'
        );


        $result["form"] = view('admin.forms.user', compact('user', 'countries', 'addresses', 'code'))->render();
        return $result;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        $user->update();

        if (count($request->address)) {
            foreach ($request->address as $key => $addr) {
                $address = Address::find($key);

                if(!$address) {
                    $address = new Address();
                    $address->user_id = $id;
                }

                $address->country_id = $addr["country_id"] ?? '';
                $address->street = $addr["street"] ?? '';
                $address->zip = $addr["zip"] ?? '';
                $address->town = $addr["town"] ?? '';
                $address->phone = $addr["phone"] ?? '';
                $address->cell = $addr["cell"] ?? '';
                $address->save();

            }
        }

//        dispatch(new SendNotificationEmailForUserUpdateProfile($user));
//        dispatch(new SendNotificationEmailForUserUpdateProfile($user))->onQueue('emails');

        return Response::json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        return Response::json($user);
    }

    private function generateRandom()
    {
        return md5(date('ljSFYhisA')) ;
    }

}
