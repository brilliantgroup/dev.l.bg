<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\AdminMenuController;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->composeNavigation();
        $this->composePageHeader();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function composeNavigation() {
        view()->composer('partials.menu', '\App\Http\Composers\NavigationComposers');
    }

    public function composePageHeader() {
        view()->composer('partials.pageheader', '\App\Http\Composers\PageHeaderComposers');
    }
}
