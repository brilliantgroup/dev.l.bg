<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitePage extends Model
{
    protected $table = 'site_pages';

    protected $fillable = [
        'page_route', 'page_name', 'page_description',
    ];
}
