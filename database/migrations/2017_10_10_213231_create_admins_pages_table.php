<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_level')->default('1');
            $table->integer('page_priority')->default('1');
            $table->integer('parent_id')->default('1')->unsigned();
            $table->integer('is_children')->default('0');
            $table->integer('is_visible')->default('1');
            $table->string('page_route', 100)->nullable();
            $table->string('page_icon', 100)->nullable();
            $table->string('page_name', 100);
            $table->text('page_description')->nullable();
            $table->integer('page_modify')->default('1');
            $table->integer('page_refresh')->default('1');
            $table->integer('page_hasoptions')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins_pages');
    }
}
