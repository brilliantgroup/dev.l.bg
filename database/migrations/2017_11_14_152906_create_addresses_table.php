<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('country_id')->unsigned()->nullable();
            $table->text('street')->nullable();
            $table->string('zip', 191)->nullable();
            $table->string('town', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->string('cell', 191)->nullable();
            $table->smallInteger('is_default')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('addresses', function($table)
        {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('countries');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
