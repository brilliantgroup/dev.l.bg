<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins_pages_permissions', function (Blueprint $table) {
            $table->integer("page_id")->unsigned();
            $table->foreign('page_id')
                ->references('id')
                ->on('admins_pages')
                ->onDelete('cascade');

            $table->integer("role_id")->unsigned();
            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');
            $table->tinyInteger("show")->default(0);
            $table->tinyInteger("options")->default(0);
            $table->tinyInteger("add")->default(0);
            $table->tinyInteger("view")->default(0);
            $table->tinyInteger("edit")->default(0);
            $table->tinyInteger("delete")->default(0);
            $table->tinyInteger("search")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins_pages_permissions');
    }
}
