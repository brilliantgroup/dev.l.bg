# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.1.77 (MySQL 5.7.20-0ubuntu0.16.04.1)
# Database: dev.l.bg
# Generation Time: 2017-12-16 19:11:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '4',
  `country_id` int(10) unsigned DEFAULT NULL,
  `street` text COLLATE utf8mb4_unicode_ci,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` smallint(6) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  KEY `addresses_country_id_foreign` (`country_id`),
  CONSTRAINT `addresses_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `user_id`, `country_id`, `street`, `zip`, `town`, `phone`, `cell`, `is_default`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,2,1,'Street','ZIP','Town','15551231231','15553213213',1,NULL,'2017-11-17 14:06:23','2017-11-17 14:06:23'),
	(108,908,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-11-29 18:01:07','2017-11-29 18:01:07'),
	(109,909,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-11-29 18:09:16','2017-11-29 18:09:16'),
	(110,4,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-11-29 18:13:23','2017-11-29 18:13:23'),
	(111,4,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-11-29 18:18:40','2017-11-29 18:18:40'),
	(112,4,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2017-11-29 18:19:36','2017-11-29 18:19:36');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;

INSERT INTO `admins` (`id`, `name`, `email`, `job_title`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Holy','oleg.lats@gmail.com','Title','$2y$10$6eE8NnyLHZ58gdgCayWkVuwkEZjgez0zNWfZvPnx0XAVa1knTfZzy','KOC5Zk26Gj74iKBVPqkwW082LyeqVw2PKO0WgBeKlJhkBbaoItM3GrQWPcz3','2017-10-06 16:29:05','2017-11-04 17:20:38'),
	(3,'vloty','victor.lototskyy@gmail.com','Title','$2y$10$Evrv9lwOfxcSrDmuUyMMveUX69p8ROPyPrikcur4WABWaPKfSP6Qq',NULL,'2017-11-17 14:18:16','2017-11-17 14:18:16'),
	(4,'Joe','test@test.com','123','$2y$10$EtwWvPVA9eBWQBNAG.XIfuXgolzlSYbSjXIi3ctrl4wBP56vhgKZC',NULL,'2017-12-11 19:54:11','2017-12-11 23:06:39');

/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admins_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins_pages`;

CREATE TABLE `admins_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_level` int(11) NOT NULL DEFAULT '1',
  `page_priority` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(10) unsigned DEFAULT '0',
  `is_children` int(11) NOT NULL DEFAULT '0',
  `is_visible` int(11) NOT NULL DEFAULT '1',
  `page_route` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `page_icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ' ',
  `page_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci,
  `page_modify` int(11) NOT NULL DEFAULT '1',
  `page_refresh` int(11) NOT NULL DEFAULT '1',
  `page_hasoptions` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins_pages` WRITE;
/*!40000 ALTER TABLE `admins_pages` DISABLE KEYS */;

INSERT INTO `admins_pages` (`id`, `page_level`, `page_priority`, `parent_id`, `is_children`, `is_visible`, `page_route`, `page_icon`, `page_name`, `page_description`, `page_modify`, `page_refresh`, `page_hasoptions`, `created_at`, `updated_at`)
VALUES
	(1,1,1,0,0,1,'admin.dashboard','glyphicon glyphicon-dashboard','Dashboard','',0,0,0,NULL,NULL),
	(2,0,1,0,0,1,'admin.profile','','My Profile','',0,0,0,NULL,NULL),
	(3,0,2,0,0,1,'admin.settings','','Settings','',0,0,0,NULL,NULL),
	(4,1,1,0,1,1,'adminpagesgroup','glyphicon glyphicon-tasks','Admin Pages',NULL,1,1,0,NULL,'2017-12-11 22:35:39'),
	(5,1,3,0,1,1,'usersgroup','fa fa-users','Users','',1,1,0,NULL,NULL),
	(6,2,3,5,0,1,'admin.users','fa fa-users','Users','',1,1,0,NULL,NULL),
	(7,1,3,0,1,1,'pagesgroup','fa fa-file-text','Site Pages',NULL,1,1,0,NULL,'2017-11-07 19:25:59'),
	(8,2,3,7,0,1,'admin.site.pages','fa fa-file-text','Site Pages','',1,1,0,NULL,NULL),
	(9,1,1,0,1,1,'databasegroup','fa fa-book','Database Info','',1,1,0,NULL,NULL),
	(10,2,1,9,0,1,'admin.countries','fa fa-book','Countries','',1,1,0,NULL,NULL),
	(11,2,2,9,0,1,'admin.categories','fa fa-folder-open','Categories','',1,1,0,NULL,NULL),
	(12,1,1,0,1,1,'productsgroup','fa fa-book','Products','',1,1,0,NULL,NULL),
	(13,2,1,12,0,1,'admin.products','fa fa-folder-open','Products','',1,1,0,NULL,NULL),
	(14,1,3,0,1,1,'adminsgroup','fa fa-user','Admins','',1,1,0,NULL,NULL),
	(15,2,3,14,0,1,'admin.admins','fa fa-user','Admins','',1,1,0,NULL,NULL),
	(16,1,1,4,0,1,'admin.pages','glyphicon glyphicon-tasks','Admin Pages',NULL,1,1,0,NULL,'2017-12-11 22:35:39'),
	(17,1,1,4,0,1,'admin.permissions','glyphicon glyphicon-tasks','Pages Permission',NULL,1,1,0,NULL,'2017-12-11 22:35:39');

/*!40000 ALTER TABLE `admins_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admins_pages_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins_pages_permissions`;

CREATE TABLE `admins_pages_permissions` (
  `page_id` int(10) unsigned DEFAULT NULL,
  `role_id` int(10) unsigned DEFAULT NULL,
  `show` tinyint(4) DEFAULT '0',
  `options` tinyint(4) DEFAULT '0',
  `add` tinyint(4) DEFAULT '0',
  `view` tinyint(4) DEFAULT '0',
  `edit` tinyint(4) DEFAULT '0',
  `delete` tinyint(4) DEFAULT '0',
  `search` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `page_id` (`page_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `admins_pages_permissions_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `admins_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `admins_pages_permissions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `admins_pages_permissions` WRITE;
/*!40000 ALTER TABLE `admins_pages_permissions` DISABLE KEYS */;

INSERT INTO `admins_pages_permissions` (`page_id`, `role_id`, `show`, `options`, `add`, `view`, `edit`, `delete`, `search`, `created_at`, `updated_at`)
VALUES
	(1,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(2,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(3,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(4,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(5,3,1,1,1,1,1,1,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(6,3,1,1,1,1,1,1,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(7,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(8,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(9,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(10,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(11,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(12,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(13,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(14,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(15,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(16,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(17,3,0,0,0,0,0,0,0,'2017-12-13 18:26:05','2017-12-13 18:26:05'),
	(1,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(2,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(3,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(4,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(5,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(6,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(7,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(8,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(9,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(10,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(11,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(12,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(13,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(14,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(15,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(16,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(17,2,1,1,1,1,1,1,0,'2017-12-14 18:36:57','2017-12-14 18:36:57'),
	(1,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(2,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(3,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(4,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(5,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(6,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(7,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(8,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(9,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(10,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(11,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(12,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(13,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(14,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(15,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(16,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31'),
	(17,1,1,1,1,1,1,1,0,'2017-12-14 19:53:31','2017-12-14 19:53:31');

/*!40000 ALTER TABLE `admins_pages_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admins_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins_roles`;

CREATE TABLE `admins_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_roles_user_id_foreign` (`user_id`),
  KEY `admin_roles_role_id_foreign` (`role_id`),
  CONSTRAINT `admin_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `admin_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admins_roles` WRITE;
/*!40000 ALTER TABLE `admins_roles` DISABLE KEYS */;

INSERT INTO `admins_roles` (`user_id`, `role_id`, `created_at`, `updated_at`)
VALUES
	(1,1,NULL,NULL),
	(3,2,NULL,NULL),
	(4,3,NULL,NULL);

/*!40000 ALTER TABLE `admins_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `active`, `deleted_at`, `created_at`, `updated_at`)
VALUES
	(1,'Category 1','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(2,'Category 2','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(3,'Category 3','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(4,'Category 4','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(5,'Category 5','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(6,'Category 6','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(7,'Category 7','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(8,'Category 8','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(9,'Category 9','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(10,'Category 10','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(11,'Category 11','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(12,'Category 12','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55'),
	(13,'Category 13','1',NULL,'2017-11-06 20:13:55','2017-11-06 20:13:55');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `priority` int(11) DEFAULT NULL,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `priority`, `code`, `name`, `created_at`, `updated_at`)
VALUES
	(1,1,'US','United States','2017-11-02 19:34:52','2017-11-03 13:09:13'),
	(2,3,'CA','Canada','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(3,NULL,'AF','Afghanistan','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(4,NULL,'AL','Albania','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(5,NULL,'DZ','Algeria','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(6,NULL,'DS','American Samoa','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(7,NULL,'AD','Andorra','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(8,NULL,'AO','Angola','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(9,NULL,'AI','Anguilla','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(10,NULL,'AQ','Antarctica','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(11,NULL,'AG','Antigua and/or Barbuda','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(12,NULL,'AR','Argentina','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(13,NULL,'AM','Armenia','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(14,NULL,'AW','Aruba','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(15,5,'AU','Australia','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(16,NULL,'AT','Austria','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(17,NULL,'AZ','Azerbaijan','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(18,NULL,'BS','Bahamas','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(19,NULL,'BH','Bahrain','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(20,NULL,'BD','Bangladesh','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(21,NULL,'BB','Barbados','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(22,NULL,'BY','Belarus','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(23,NULL,'BE','Belgium','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(24,NULL,'BZ','Belize','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(25,NULL,'BJ','Benin','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(26,NULL,'BM','Bermuda','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(27,NULL,'BT','Bhutan','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(28,NULL,'BO','Bolivia','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(29,NULL,'BA','Bosnia and Herzegovina','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(30,NULL,'BW','Botswana','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(31,NULL,'BV','Bouvet Island','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(32,NULL,'BR','Brazil','2017-11-02 19:34:52','2017-11-02 19:34:52'),
	(33,NULL,'IO','British lndian Ocean Territory','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(34,NULL,'BN','Brunei Darussalam','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(35,NULL,'BG','Bulgaria','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(36,NULL,'BF','Burkina Faso','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(37,NULL,'BI','Burundi','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(38,NULL,'KH','Cambodia','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(39,NULL,'CM','Cameroon','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(40,NULL,'CV','Cape Verde','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(41,NULL,'KY','Cayman Islands','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(42,NULL,'CF','Central African Republic','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(43,NULL,'TD','Chad','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(44,NULL,'CL','Chile','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(45,NULL,'CN','China','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(46,NULL,'CX','Christmas Island','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(47,NULL,'CC','Cocos (Keeling) Islands','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(48,NULL,'CO','Colombia','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(49,NULL,'KM','Comoros','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(50,NULL,'CG','Congo','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(51,NULL,'CK','Cook Islands','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(52,NULL,'CR','Costa Rica','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(53,NULL,'HR','Croatia (Hrvatska)','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(54,NULL,'CU','Cuba','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(55,NULL,'CY','Cyprus','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(56,NULL,'CZ','Czech Republic','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(57,NULL,'DK','Denmark','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(58,NULL,'DJ','Djibouti','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(59,NULL,'DM','Dominica','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(60,NULL,'DO','Dominican Republic','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(61,NULL,'TP','East Timor','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(62,NULL,'EC','Ecudaor','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(63,NULL,'EG','Egypt','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(64,NULL,'SV','El Salvador','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(65,NULL,'GQ','Equatorial Guinea','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(66,NULL,'ER','Eritrea','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(67,NULL,'EE','Estonia','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(68,NULL,'ET','Ethiopia','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(69,NULL,'FK','Falkland Islands (Malvinas)','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(70,NULL,'FO','Faroe Islands','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(71,NULL,'FJ','Fiji','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(72,NULL,'FI','Finland','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(73,NULL,'FR','France','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(74,NULL,'FX','France, Metropolitan','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(75,NULL,'GF','French Guiana','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(76,NULL,'PF','French Polynesia','2017-11-02 19:34:53','2017-11-02 19:34:53'),
	(77,NULL,'TF','French Southern Territories','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(78,NULL,'GA','Gabon','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(79,NULL,'GM','Gambia','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(80,NULL,'GE','Georgia','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(81,6,'DE','Germany','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(82,NULL,'GH','Ghana','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(83,NULL,'GI','Gibraltar','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(84,NULL,'GR','Greece','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(85,NULL,'GL','Greenland','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(86,NULL,'GD','Grenada','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(87,NULL,'GP','Guadeloupe','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(88,NULL,'GU','Guam','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(89,NULL,'GT','Guatemala','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(90,NULL,'GN','Guinea','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(91,NULL,'GW','Guinea-Bissau','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(92,NULL,'GY','Guyana','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(93,NULL,'HT','Haiti','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(94,NULL,'HM','Heard and Mc Donald Islands','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(95,NULL,'HN','Honduras','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(96,NULL,'HK','Hong Kong','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(97,NULL,'HU','Hungary','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(98,NULL,'IS','Iceland','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(99,NULL,'IN','India','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(100,NULL,'ID','Indonesia','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(101,NULL,'IR','Iran (Islamic Republic of)','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(102,NULL,'IQ','Iraq','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(103,NULL,'IE','Ireland','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(104,NULL,'IL','Israel','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(105,NULL,'IT','Italy','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(106,NULL,'CI','Ivory Coast','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(107,NULL,'JM','Jamaica','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(108,NULL,'JP','Japan','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(109,NULL,'JO','Jordan','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(110,NULL,'KZ','Kazakhstan','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(111,NULL,'KE','Kenya','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(112,NULL,'KI','Kiribati','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(113,NULL,'KP','Korea, Democratic People\'s Republic of','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(114,NULL,'KR','Korea, Republic of','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(115,NULL,'KW','Kuwait','2017-11-02 19:34:54','2017-11-02 19:34:54'),
	(116,NULL,'KG','Kyrgyzstan','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(117,NULL,'LA','Lao People\'s Democratic Republic','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(118,NULL,'LV','Latvia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(119,NULL,'LB','Lebanon','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(120,NULL,'LS','Lesotho','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(121,NULL,'LR','Liberia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(122,NULL,'LY','Libyan Arab Jamahiriya','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(123,NULL,'LI','Liechtenstein','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(124,NULL,'LT','Lithuania','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(125,NULL,'LU','Luxembourg','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(126,NULL,'MO','Macau','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(127,NULL,'MK','Macedonia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(128,NULL,'MG','Madagascar','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(129,NULL,'MW','Malawi','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(130,NULL,'MY','Malaysia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(131,NULL,'MV','Maldives','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(132,NULL,'ML','Mali','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(133,NULL,'MT','Malta','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(134,NULL,'MH','Marshall Islands','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(135,NULL,'MQ','Martinique','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(136,NULL,'MR','Mauritania','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(137,NULL,'MU','Mauritius','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(138,NULL,'TY','Mayotte','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(139,NULL,'MX','Mexico','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(140,NULL,'FM','Micronesia, Federated States of','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(141,NULL,'MD','Moldova, Republic of','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(142,NULL,'MC','Monaco','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(143,NULL,'MN','Mongolia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(144,NULL,'MS','Montserrat','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(145,NULL,'MA','Morocco','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(146,NULL,'MZ','Mozambique','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(147,NULL,'MM','Myanmar','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(148,NULL,'NA','Namibia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(149,NULL,'NR','Nauru','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(150,NULL,'NP','Nepal','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(151,NULL,'NL','Netherlands','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(152,NULL,'AN','Netherlands Antilles','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(153,NULL,'NC','New Caledonia','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(154,NULL,'NZ','New Zealand','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(155,NULL,'NI','Nicaragua','2017-11-02 19:34:55','2017-11-02 19:34:55'),
	(156,NULL,'NE','Niger','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(157,NULL,'NG','Nigeria','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(158,NULL,'NU','Niue','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(159,NULL,'NF','Norfork Island','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(160,NULL,'MP','Northern Mariana Islands','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(161,NULL,'NO','Norway','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(162,NULL,'OM','Oman','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(163,NULL,'PK','Pakistan','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(164,NULL,'PW','Palau','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(165,NULL,'PA','Panama','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(166,NULL,'PG','Papua New Guinea','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(167,NULL,'PY','Paraguay','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(168,NULL,'PE','Peru','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(169,NULL,'PH','Philippines','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(170,NULL,'PN','Pitcairn','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(171,NULL,'PL','Poland','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(172,NULL,'PT','Portugal','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(173,NULL,'PR','Puerto Rico','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(174,NULL,'QA','Qatar','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(175,NULL,'RE','Reunion','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(176,NULL,'RO','Romania','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(177,NULL,'RU','Russian Federation','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(178,NULL,'RW','Rwanda','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(179,NULL,'KN','Saint Kitts and Nevis','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(180,NULL,'LC','Saint Lucia','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(181,NULL,'VC','Saint Vincent and the Grenadines','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(182,NULL,'WS','Samoa','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(183,NULL,'SM','San Marino','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(184,NULL,'ST','Sao Tome and Principe','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(185,NULL,'SA','Saudi Arabia','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(186,NULL,'SN','Senegal','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(187,NULL,'SC','Seychelles','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(188,NULL,'SL','Sierra Leone','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(189,NULL,'SG','Singapore','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(190,NULL,'SK','Slovakia','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(191,NULL,'SI','Slovenia','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(192,NULL,'SB','Solomon Islands','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(193,NULL,'SO','Somalia','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(194,NULL,'ZA','South Africa','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(195,NULL,'GS','South Georgia South Sandwich Islands','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(196,NULL,'ES','Spain','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(197,NULL,'LK','Sri Lanka','2017-11-02 19:34:56','2017-11-02 19:34:56'),
	(198,NULL,'SH','St. Helena','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(199,NULL,'PM','St. Pierre and Miquelon','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(200,NULL,'SD','Sudan','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(201,NULL,'SR','Suriname','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(202,NULL,'SJ','Svalbarn and Jan Mayen Islands','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(203,NULL,'SZ','Swaziland','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(204,NULL,'SE','Sweden','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(205,NULL,'CH','Switzerland','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(206,NULL,'SY','Syrian Arab Republic','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(207,NULL,'TW','Taiwan','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(208,NULL,'TJ','Tajikistan','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(209,NULL,'TZ','Tanzania, United Republic of','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(210,NULL,'TH','Thailand','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(211,NULL,'TG','Togo','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(212,NULL,'TK','Tokelau','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(213,NULL,'TO','Tonga','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(214,NULL,'TT','Trinidad and Tobago','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(215,NULL,'TN','Tunisia','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(216,NULL,'TR','Turkey','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(217,NULL,'TM','Turkmenistan','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(218,NULL,'TC','Turks and Caicos Islands','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(219,NULL,'TV','Tuvalu','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(220,NULL,'UG','Uganda','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(221,NULL,'UA','Ukraine','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(222,NULL,'AE','U.A.E.','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(223,4,'GB','U.K.','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(224,NULL,'UM','United States minor outlying islands','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(225,NULL,'UY','Uruguay','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(226,NULL,'UZ','Uzbekistan','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(227,NULL,'VU','Vanuatu','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(228,NULL,'VA','Vatican City State','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(229,NULL,'VE','Venezuela','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(230,NULL,'VN','Vietnam','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(231,NULL,'VG','Virigan Islands (British)','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(232,NULL,'VI','Virgin Islands (U.S.)','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(233,NULL,'WF','Wallis and Futuna Islands','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(234,NULL,'EH','Western Sahara','2017-11-02 19:34:57','2017-11-02 19:34:57'),
	(235,NULL,'YE','Yemen','2017-11-02 19:34:58','2017-11-02 19:34:58'),
	(236,NULL,'YU','Yugoslavia','2017-11-02 19:34:58','2017-11-02 19:34:58'),
	(237,NULL,'ZR','Zaire','2017-11-02 19:34:58','2017-11-02 19:34:58'),
	(238,NULL,'ZM','Zambia','2017-11-02 19:34:58','2017-11-02 19:34:58'),
	(239,NULL,'ZW','Zimbabwe','2017-11-02 19:34:58','2017-11-02 19:34:58'),
	(240,2,'WW','Worldwide','2017-11-02 19:34:58','2017-11-02 19:34:58');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2017_10_03_203437_create_admins_table',1),
	(5,'2017_10_10_213231_create_admins_pages_table',2),
	(6,'2017_11_01_141246_create_site_pages_table',3),
	(9,'2017_11_02_190135_create_countries_table',4),
	(10,'2017_11_06_194323_create_categories_table',5),
	(11,'2017_11_06_204905_create_products_table',6),
	(13,'2017_11_14_152906_create_addresses_table',7),
	(14,'2017_11_23_173625_create_jobs_table',8),
	(15,'2017_11_23_200714_create_tests_table',9),
	(16,'2017_12_08_205945_create_admin_roles_table',10);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`)
VALUES
	(1,'superadmin','Superadmin',NULL,NULL),
	(2,'admin','Admin',NULL,'2017-12-13 16:10:21'),
	(3,'guest','Guest',NULL,NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table site_pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `site_pages`;

CREATE TABLE `site_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_route` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `site_pages` WRITE;
/*!40000 ALTER TABLE `site_pages` DISABLE KEYS */;

INSERT INTO `site_pages` (`id`, `page_route`, `page_name`, `page_description`, `created_at`, `updated_at`)
VALUES
	(1,'aboutUs','About Us','Lorem...',NULL,'2017-11-01 18:36:02'),
	(2,'impressum','Impressum','Lorem...',NULL,'2017-11-01 18:27:14'),
	(3,'datenschutz','Datenschutz','Lorem...',NULL,NULL);

/*!40000 ALTER TABLE `site_pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tests`;

CREATE TABLE `tests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;

INSERT INTO `tests` (`id`, `user`, `created_at`, `updated_at`)
VALUES
	(1,189,'2017-11-23 20:27:42','2017-11-23 20:27:42'),
	(2,190,'2017-11-23 20:28:34','2017-11-23 20:28:34'),
	(3,189,'2017-11-23 20:28:43','2017-11-23 20:28:43'),
	(4,12,'2017-11-29 18:15:57','2017-11-29 18:15:57'),
	(5,123,'2017-11-29 18:17:47','2017-11-29 18:17:47');

/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'Holy','oleg.lats@gmail.com','$2y$10$uDNjQvBoixdjRnuMXuvXoOxlgA5K7VppSvLdwOyae9miJItyCaf0q','WRRfPkk6iU9WxePPAKSAHDw8Z45QzedSZL3PRRrLGvl9ldOa8vTDRDAU7reY','2017-10-06 16:28:19','2017-10-06 16:28:19'),
	(3,'Prof. Julia Bogisich IVa','haylee.larsona@example.com','$2y$10$GtaqnEaVN2vgnqzx2JR9W.pxfDQgqwImHpNiOjhh1KIOSB3ygTa0y','MILg99onGH','2017-10-26 19:19:37','2017-11-04 16:43:13'),
	(4,'Prof. Vergie Brakus III','priscilla82@example.com','$2y$10$GtaqnEaVN2vgnqzx2JR9W.pxfDQgqwImHpNiOjhh1KIOSB3ygTa0y','qU255x7pAa','2017-10-26 19:19:37','2017-10-26 19:19:37'),
	(189,'Test1','t1@t1.com','$2y$10$yptjlT1z4bRUqsbVosOPueSci8cWNxlyUp6hGTdr7eyL4VocMF9ES',NULL,'2017-11-16 17:35:02','2017-11-16 17:35:02'),
	(190,'Test2','t2@t2.com','$2y$10$yptjlT1z4bRUqsbVosOPueSci8cWNxlyUp6hGTdr7eyL4VocMF9ES',NULL,'2017-11-16 17:35:02','2017-11-16 17:35:02'),
	(191,'Test3','t3@t3.com','$2y$10$yptjlT1z4bRUqsbVosOPueSci8cWNxlyUp6hGTdr7eyL4VocMF9ES',NULL,'2017-11-16 17:35:02','2017-11-16 17:35:02'),
	(192,'t5@t5.t5','t5@t5.com','$2y$10$xOYUNg2hwjyBb..JJBpgO.4l6/9JV2aS205B11EAto4eIM9l5FRxm',NULL,'2017-11-16 18:37:16','2017-11-16 18:37:16'),
	(908,'test@test.com','test@test.com','$2y$10$dJr3E0U8Lo7GlYC978BMbeVBbwyZA3T.MGGCi.LLpb3UCO1HH0.fe',NULL,'2017-11-29 18:01:07','2017-11-29 18:01:07'),
	(909,'wer','wer@wer.com','$2y$10$3k3rZBI8YlPYdtLvAxEO1uFzqWO7Bc0krqAU7RJurzh8iifQ9rOdC',NULL,'2017-11-29 18:07:18','2017-11-29 18:07:18');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
