<?php

use Illuminate\Database\Seeder;

class AddDatabasePageToAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $_pages = array(
            array(
                "page_level" => "1",
                "page_priority" => "1",
                'parent_id' => "0",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "databasegroup",
                "page_icon" => "fa fa-book",
                "page_name" => "Database Info",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "2",
                "page_priority" => "1",
                'parent_id' => "9",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "admin.countries",
                "page_icon" => "fa fa-book",
                "page_name" => "Countries",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),

        );


        foreach ($_pages as $key => $page) {
            DB::table('admins_pages')->insert($page);
        }
    }
}
