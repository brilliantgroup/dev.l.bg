<?php

use Illuminate\Database\Seeder;

class AdminPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $_pages = array(
            array(
                "page_level" => "1",
                "page_priority" => "1",
                "parent_id" => "0",
                "is_children" => "0",
                "is_visible" => "1",
                "page_route" => "admin.dashboard",
                "page_icon" => "glyphicon glyphicon-dashboard",
                "page_name" => "Dashboard",
                "page_description" => "",
                "page_modify" => "0",
                "page_refresh" => "0",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "0",
                "page_priority" => "1",
                "parent_id" => "0",
                "is_children" => "0",
                "is_visible" => "1",
                "page_route" => "admin.profile",
                "page_icon" => "",
                "page_name" => "My Profile",
                "page_description" => "",
                "page_modify" => "0",
                "page_refresh" => "0",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "0",
                "page_priority" => "2",
                "parent_id" => "0",
                "is_children" => "0",
                "is_visible" => "1",
                "page_route" => "admin.settings",
                "page_icon" => "",
                "page_name" => "Settings",
                "page_description" => "",
                "page_modify" => "0",
                "page_refresh" => "0",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "1",
                "page_priority" => "2",
                "is_children" => "0",
                "is_visible" => "1",
                "parent_id" => "0",
                "page_route" => "admin.pages",
                "page_icon" => "glyphicon glyphicon-tasks",
                "page_name" => "Admin Pages",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "1",
                "page_priority" => "3",
                "parent_id" => "0",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "usersgroup",
                "page_icon" => "fa fa-users",
                "page_name" => "Users",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "2",
                "page_priority" => "3",
                "parent_id" => "5",
                "is_children" => "0",
                "is_visible" => "1",
                "page_route" => "admin.users",
                "page_icon" => "fa fa-users",
                "page_name" => "Users",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "1",
                "page_priority" => "3",
                "parent_id" => "0",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "pagesgroup",
                "page_icon" => "fa fa-users",
                "page_name" => "Site Pages",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "2",
                "page_priority" => "3",
                "parent_id" => "7",
                "is_children" => "0",
                "is_visible" => "1",
                "page_route" => "admin.site.pages",
                "page_icon" => "fa fa-file-text",
                "page_name" => "Site Pages",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),

            array(
                "page_level" => "1",
                "page_priority" => "1",
                'parent_id' => "0",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "databasegroup",
                "page_icon" => "fa fa-book",
                "page_name" => "Database Info",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "2",
                "page_priority" => "1",
                'parent_id' => "9",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "admin.countries",
                "page_icon" => "fa fa-book",
                "page_name" => "Countries",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
            array(
                "page_level" => "2",
                "page_priority" => "2",
                'parent_id' => "9",
                "is_children" => "1",
                "is_visible" => "1",
                "page_route" => "admin.categories",
                "page_icon" => "fa fa-folder-open",
                "page_name" => "Categories",
                "page_description" => "",
                "page_modify" => "1",
                "page_refresh" => "1",
                "page_hasoptions" => "0"
            ),
        );


        foreach ($_pages as $key => $page) {
            DB::table('admins_pages')->insert($page);
        }
    }
}
