<?php

use Illuminate\Database\Seeder;

class AdminSitePagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $_pages = array(
            array(
                "page_route" => "aboutUs",
                "page_name" => "About Us",
                "page_description" => "Lorem..."
            ),
            array(
                "page_route" => "impressum",
                "page_name" => "Impressum",
                "page_description" => "Lorem..."
            ),
            array(
                "page_route" => "datenschutz",
                "page_name" => "Datenschutz",
                "page_description" => "Lorem..."
            )
        );

        foreach ($_pages as $key => $page) {
            DB::table('site_pages')->insert($page);
        }
    }
}
