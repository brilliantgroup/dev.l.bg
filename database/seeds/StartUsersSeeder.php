<?php

use Illuminate\Database\Seeder;

class StartUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
          'name' => 'Holy',
          'email' => 'oleg.lats@gmail.com',
          'password' => bcrypt('qwerty'),
        ]);

        DB::table('admins')->insert([
          'name' => 'Holy',
          'email' => 'oleg.lats@gmail.com',
          'password' => bcrypt('qwerty'),
        ]);
    }
}
