<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create();

//        factory(App\User::class, 1)->create()->each(function($user){
//            $user->save();
//            $user->addresses()->save(factory(App\Address::class,2)->make());
//            $user->addresses()->save(factory(App\Address::class)->make());
//            factory(App\Address::class)->create();
//        });

//        factory(App\Address::class, 10)->create();

    }
}
