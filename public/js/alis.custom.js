// for datatables
var dataTable;

$(document).ready(function () {
    // active menu
    $("#navmenu li ul.children li.active").closest('ul').closest('li').addClass('nav-active');
    $("#navmenu li ul.children li.active").closest('ul.children').show();

    $("#myModal").on("click", "#btn-save", function () {
        update();
        return false;
    });

})

$(".profile").on("click", function () {
    url = this.rel;
    getForm(url);
    $('#fa').trigger("reset");
    return false;
})


$(".add_new_record").on("click", function () {
    url = this.rel;
    getForm(url);
    $('#fa').trigger("reset");
    return false;
})

$(".refresh").on("click", function () {
    dataTable.ajax.reload();
    return false;
})

function getForm(url, id) {

    var id = id || '';
    var url = url;

    if (id) {
        url = url + "/id/" + id;
    }

    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        beforeSend: function () {
            loading(true);
        },
        success: function (data) {
            loading();

            $("#myModalLabel").html(data.title);
            $(".modal-body").html(data.form);

            $(".popup_button").empty();
            $.each(data.buttons, function (index, value) {
                $(".popup_button").append(value);
            });

            $('#myModal').modal('show');

        },
        error: function (data) {
            showError();
        }
    });


    return false;
}

function update() {
    if ($("#fa").validate({
//    ignore: "",
//            ignore: "hidden:not(select)",
            ignore: "hidden",
            errorClass: "error"
        }).form()) {

        var url = $("#fa").attr("action") ? $("#fa").attr("action") : document.location;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

        })

        var formData = $("#fa").serialize();

        //used to determine the http verb to use [add=POST], [update=PUT]
        var state = ($('#_id').val()) ? 'update' : 'add';

        var type = "POST"; //for creating new resource
        var item_id = $('#_id').val();
        var my_url = url;

        if (state == "update") {
            type = "PUT"; //for updating existing resource
            //my_url += '/id/' + item_id;
        }

        $.ajax({
            type: type,
            url: my_url,
            data: formData,
            dataType: 'json',
            beforeSend: function () {
                //$(".popup_button .button.submit").attr("disabled", "disabled");
                notification("Connection", "Update data...", "growl-warning");
            },
            success: function (data) {
                if (data.id) {
                    closePopup();
                    dataTable.ajax.reload();
                    notification("Notification", "Record added/updated successfully", "growl-success");
                } else if (data.redirect) {
                    window.location.replace(document.location);
                } else {
                    closePopup();
                    notification("Notification", "Record added/updated successfully", "growl-success");
                }

                resetPopup();
                //console.log(data);
            },
            error: function (data) {
                notification("Notification", "We got errors, something wrong:(", "growl-danger");
                //console.log('Error:', data);
                //console.log('Error:', data.responseText);

                var response = JSON.parse(data.responseText);
                displayErrorMsg(response.errors);

            }
        });
    } else {
        notification("Notification", "Please fill required fields.", "growl-danger");
    }


};

function displayErrorMsg(msg) {
    $(".print-error-msg").find("ul").html('');
    $(".print-error-msg").css('display', 'block');
    $.each(msg, function (key, value) {
        $(".print-error-msg").find("ul").append('<li>' + value + '</li>');
    });
}


var iURL = '';

function closePopup(refresh) {
    $('#myModal').modal('hide');
}

function bind() {
    $(".view").unbind().on("click", function () {
        alert('asd');
        id = $(this).attr("rel");
        url = document.location.pathname;
        getForm(url, id);
        return false;
    })


    $(".update").unbind().on("click", function () {
        id = $(this).attr("rel");
        url = document.location.pathname;
        getForm(url, id);
        return false;
    })

    $(".delete").unbind().on("click", function () {
        if (confirm("Are you sure?")) {

            id = $(this).attr("rel");
            var isDeleted = delRecord(id);


        }
        return false;
    })
}

function delRecord(id) {
    var item_id = id || '';
    var url = document.location.pathname;
    var result;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    })

    $.ajax({

        type: "DELETE",
        url: url + '/' + item_id,
        beforeSend: function () {
            loading(true);
        },
        success: function (data) {
            loading();
            console.log(data);

            if (data.redirect) {
                window.location.replace(document.location);
            } else if (data === 1) {
                dataTable.ajax.reload();
                notification("Notification", "Record successfully removed", "growl-success");
            } else {
                notification("Notification", "We got errors, something wrong:(", "growl-danger");
            }
        },
        error: function (data) {
            loading();
            notification("Notification", "We got errors, something wrong:(", "growl-danger");
            console.log('Error:', data);
        }
    });


}


function showError() {
    $("#myModalLabel").html("Error");
    $(".modal-body").html('<p class="norecords"><span class="smile">:(</span><br> Sorry, we got error. Please try later.</p>');
    $(".popup_note").html("Please <a href='javascript:;' onclick='closePopup();'>click here</a> for continue.");
}

function resetPopup() {
    $("#myModel .modal-title").html();
    $("#myModel .modal-body").html();
    $("#myModel .modal-footer").html();
}

//popups.
function initPopUp(id) {
    var _id = "#" + id;

    if (!$(_id).length) {
        var _popup = $("#myModal").clone().prop("id", id);
    } else {
        var _popup = $(_id);
    }

    $(_popup).find(".modal-title").html('...');
    $(_popup).find(".modal-body").empty();
    return (_popup);
}


function notification(title, text, class_name) {
    jQuery.gritter.add({
        title: title,
        text: text,
        class_name: class_name,
        image: false,
        sticky: false,
        time: ''
    });
}

function loading(status) {
    if (status) {
        $("#loading").show();
    } else {
        $("#loading").hide();
    }
}
