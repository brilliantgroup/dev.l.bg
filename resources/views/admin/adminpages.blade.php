@extends('layouts.admin')

@section('content')
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="table_adminpages">
            <thead>
            <tr>
                <th>Name</th>
                <th width="100">Action</th>
            </tr>
            </thead>
            <tbody id="pages">
            @if(count($pages))
                @foreach($pages as $page)
                    <tr>
                        <td><h5>
                                <i class="{{$page->page_icon}}"></i>&nbsp;&nbsp;{{ $page->page_name }}
                            </h5>
                        </td>
                        <td class="table-action" valign='middle'><a class="update" alt="Update record"
                                                                    href="javascript:;"
                                                                    rel="{{ $page->id }}"><i
                                        class="fa fa-pencil"></i></a>
                            <a class="delete-row delete" alt="Remove record" href="javascript:;"
                               rel="{{ $page->id }}"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @if(count($page->childs))
                        @include('components.manageChild',['childs' => $page->childs])
                    @endif
                @endforeach
            @endif

            </tbody>
        </table>
    </div>

    <script>
        $(document).ready(function () {
            bind();
        });
    </script>
@endsection