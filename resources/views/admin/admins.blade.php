@extends('layouts.admin')

@section('content')
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="table_admins">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100">Action</th>
            </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
    </div>

    <script>


        $(document).ready(function () {
            dataTable = $('#table_admins').DataTable({
                "sPaginationType": "full_numbers",
                processing: true,
                serverSide: true,
                ajax: '{!! route('get.admins') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {"data": "action", className: "table-action"}
                ],
                columnDefs: [
                    {orderable: false, targets: [0, -1]}
                ],
                "drawCallback": function (settings) {
//                bindTable();
                    bind();
                },
                "aLengthMenu": [
                    [20, 40, 60, 100],
                    [20, 40, 60, 100],
                ]
            });
        });
    </script>
@endsection