{!! Form::model($page, ["id"=>"fa", "name"=>"fa"]) !!}

<input name="_id" id="_id" type="hidden" value="{{ $page->id }}">
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_route">Parent Page</label>
    <div class="col-sm-8">

        {!! Form::select('parent_id', $pages, $page->parent_id, ['class'=>'form-control chosen-select', 'placeholder' => 'Select Parent...']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_route">Page Route <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('page_route', null, ['class' => 'form-control', 'placeholder'=>'Type Page Route', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_name">Page Name <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('page_name', null, ['class' => 'form-control', 'placeholder'=>'Type Name', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_icon">Page Icon </label>
    <div class="col-sm-8">
        {!! Form::text('page_icon', null, ['class' => 'form-control', 'placeholder'=>'Type Name']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_name">Page Priority <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('page_priority', null, ['class' => 'form-control col-sm-2', 'placeholder'=>'Type Name', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="is_visible">Is Visible <span class="asterisk">*</span></label>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="is_visible"
                   id="is_visible1"
                   value="1" <?=($page->is_visible == 1) ? 'checked' : ''?>>
            <label for="is_visible1">On</label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="is_visible"
                   id="is_visible2"
                   value="0" <?=($page->is_visible == 0) ? 'checked' : ''?>>
            <label for="is_visible2">Off</label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_modify">Modify Page <span class="asterisk">*</span></label>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_modify"
                   id="page_modify1"
                   value="1" <?=($page->page_modify == 1) ? 'checked' : ''?>>
            <label for="page_modify1">On</label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_modify"
                   id="page_modify2"
                   value="0" <?=($page->page_modify == 0) ? 'checked' : ''?>>
            <label for="page_modify2">Off</label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_refresh">Page Refresh <span class="asterisk">*</span></label>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_refresh"
                   id="page_refresh1"
                   value="1" <?=($page->page_refresh == 1) ? 'checked' : ''?>>
            <label for="page_refresh1">On</label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_refresh"
                   id="page_refresh2"
                   value="0" <?=($page->page_refresh == 0) ? 'checked' : ''?>>
            <label for="page_refresh2">Off</label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_hasoptions">Has Options<span class="asterisk">*</span></label>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_hasoptions"
                   id="page_hasoption1"
                   value="1" <?=($page->page_hasoptions == 1) ? 'checked' : ''?>>
            <label for="page_hasoption1">On</label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="rdio rdio-primary">
            <input type="radio" name="page_hasoptions"
                   id="page_hasoption2"
                   value="0" <?=($page->page_hasoptions == 0) ? 'checked' : ''?>>
            <label for="page_hasoption2">Off</label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        {!! Form::textarea('page_description', null, ['rows'=>'10','id'=>'wysiwyg', 'class' => 'form-control', 'placeholder'=>'Enter text here...']) !!}
    </div>
</div>
{!! Form::close() !!}
<style>
    .modal-dialog {
        width: 700px;
    }
</style>

<script>
    $(document).ready(function () {
        $(".chosen-select").chosen({'width': '100%', 'white-space': 'nowrap'});
    });
</script>
