{!! Form::model($category, ["id"=>"fa", "name"=>"fa"]) !!}

<input name="_id" id="_id" type="hidden" value="{{ $category->id }}">
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label" for="name">Name <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Type Name', 'required']) !!}
    </div>
</div>

{!! Form::close() !!}

