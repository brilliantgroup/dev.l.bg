{!! Form::model($country, ["id"=>"fa", "name"=>"fa"]) !!}

<div class="panel">
    <div class="panel-body">

        <input name="_id" id="_id" type="hidden" value="{{ $country->id }}">
        <div class="alert alert-danger print-error-msg" style="display:none">
            <ul></ul>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label" for="name">Name <span class="asterisk">*</span></label>
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Type Name', 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="code">Code <span class="asterisk">*</span></label>
            <div class="col-sm-8">
                {!! Form::text('code', null, ['class' => 'form-control', 'placeholder'=>'Type Code', 'required']) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::close() !!}

