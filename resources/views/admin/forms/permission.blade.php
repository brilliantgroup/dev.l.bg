{!! Form::model($role, ["id"=>"fa", "name"=>"fa"]) !!}
<fieldset>
    <legend>Info</legend>
    {{--<p class="lead"></p>--}}
    <div class="panel">
        <div class="panel-body">
            <input name="_id" id="_id" type="hidden" value="{{ $role->id }}">
            <div class="alert alert-danger print-error-msg" style="display:none">
                <ul></ul>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label" for="display_name">Display Name <span
                            class="asterisk">*</span></label>
                <div class="col-sm-8">
                    {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder'=>'Type Display Name'], 'required') !!}
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label" for="name">Name <span class="asterisk">*</span></label>
                <div class="col-sm-8"><h5 class="text-danger">{{$role->name}}</h5>
                </div>
            </div>

        </div>
    </div>
    <legend>Info</legend>
    <div class="panel">
        <div class="panel-body">
            <table class="table table-stripped table-hover" id="table_permissions">
                <thead>
                <tr>
                    <th>Page Name</th>
                    <th class="text-center">All</th>
                    <th class="text-center">Show</th>
                    <th class="text-center">Options</th>
                    <th class="text-center">Add</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages as $page)
                    <input type="hidden" name="pages[]" value="{{$page->id}}">
                    <tr class="text-center">
                        <td class="text-left">{{$page->page_name}}</td>
                        <td><input class="row-all" type="checkbox" name="all[{{$page->id}}]" value="1"></td>
                        <td><input class="ch" type="checkbox" name="show[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->show)??0) ? 'checked' : '' ?>></td>
                        <td><input class="ch" type="checkbox" name="options[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->options)??0) ? 'checked' : '' ?>></td>
                        <td><input class="ch" type="checkbox" name="add[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->add)??0) ? 'checked' : '' ?>></td>
                        <td><input class="ch" type="checkbox" name="view[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->view)??0) ? 'checked' : '' ?>></td>
                        <td><input class="ch" type="checkbox" name="edit[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->edit)??0) ? 'checked' : '' ?>></td>
                        <td><input class="ch" type="checkbox" name="delete[{{$page->id}}]"
                                   value="1" <?=(($permissions[$page->id]->delete)??0) ? 'checked' : '' ?>></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .modal-dialog {
            width: 700px;
        }
    </style>

    <script>
        $(document).ready(function () {
            $(".row-all").on("click", function () {
                if ($(this).prop("checked")) {
                    $(this).parent().parent().find("input").prop("checked", true);
                } else {
                    $(this).parent().parent().find("input").prop("checked", false);
                }
            });

            $("#table_permissions tr td input:not(:first)").on('click', function () {
                $el = this;
                checkCheckbox(this);
            })

            $("#table_permissions tr td input").each(function () {
                $el = this;
                checkCheckbox(this);
            })

            $(".chosen-select").chosen({'width': '100%', 'white-space': 'nowrap'});
        });

        function checkCheckbox($el) {
            count_checked = $($el).parent().parent().find("input.ch:checked").length;
            $($el).parent().parent().find("input.row-all").prop("checked", (count_checked == 6));
        }
    </script>

