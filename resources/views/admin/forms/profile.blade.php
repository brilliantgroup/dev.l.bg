{!! Form::model($user, ["id"=>"fa", "name"=>"fa"]) !!}

<input name="_id" id="_id" type="hidden" value="{{ $user->id }}">
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>


<div class="form-group">
    <label class="col-sm-4 control-label" for="name">Name <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Type Name']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="email">Email <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'Type Email', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label"
           for="password">Password <?=($user->id) ? '' : '<span class="asterisk">*</span>'?></label>
    <div class="col-sm-8">
        <?
            $required = ($user->id) ? '' : 'required';
        ?>
        {!! Form::password('password', ['class' => 'form-control', 'placeholder'=>'Type Password', $required]) !!}
    </div>
</div>
{!! Form::close() !!}
