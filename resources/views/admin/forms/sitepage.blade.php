{!! Form::model($page, ["id"=>"fa", "name"=>"fa"]) !!}

<input name="_id" id="_id" type="hidden" value="{{ $page->id }}">
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label" for="page_route">Page route <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('page_route', null, ['class' => 'form-control', 'placeholder'=>'Type Page Route', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label" for="page_name">Page Name <span class="asterisk">*</span></label>
    <div class="col-sm-8">
        {!! Form::text('page_name', null, ['class' => 'form-control', 'placeholder'=>'Type Name', 'required']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        {!! Form::textarea('page_description', null, ['rows'=>'10','id'=>'wysiwyg', 'class' => 'form-control', 'placeholder'=>'Enter text here...']) !!}
    </div>
</div>
<style>
    .modal-dialog {
        width: 950px;
    }
</style>

<script>
    $(document).ready(function () {
        $('#wysiwyg').wysihtml5({color: false, html: true});
    });
</script>
