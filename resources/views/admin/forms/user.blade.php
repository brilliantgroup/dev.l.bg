{!! Form::model($user, ["id"=>"fa", "name"=>"fa"]) !!}

<input name="_id" id="_id" type="hidden" value="{{ $user->id }}">
<div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div>

<ul class="nav nav-tabs">
    <li class="active"><a href="#info" data-toggle="tab"><strong>Info</strong></a></li>
    <li><a href="#address" data-toggle="tab"><strong>Address</strong></a></li>
</ul>


<div class="tab-content">
    <div class="tab-pane active" id="info">
        <div class="form-group">
            <label class="col-sm-4 control-label" for="name">Name <span class="asterisk">*</span></label>
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Type Name']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="email">Email <span class="asterisk">*</span></label>
            <div class="col-sm-8">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder'=>'Type Email', 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label"
                   for="password">Password <?=($user->id) ? '' : '<span class="asterisk">*</span>'?></label>
            <div class="col-sm-8">
                <?
                $required = ($user->id) ? '' : 'required';
                ?>
                {!! Form::password('password', ['class' => 'form-control', 'placeholder'=>'Type Password', $required]) !!}
            </div>
        </div>
    </div>
    <div class="tab-pane" id="address">
        @if (count($addresses))
             @foreach($addresses as $key=>$address)
                <?
                    $code = ($code)?$code:$address->id;
                ?>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][country_id]">Country</label>
                    <div class="col-sm-8">
                        {!! Form::select('address['.$code.'][country_id]', $countries, $address->country_id, ['class'=>'form-control chosen-select', 'placeholder' => 'Select country...']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][street]">Street</label>
                    <div class="col-sm-8">
                        {!! Form::text('address['.$code.'][street]', $address->street, ['class' => 'form-control', 'placeholder'=>'Type Street']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][town]">Town</label>
                    <div class="col-sm-8">
                        {!! Form::text('address['.$code.'][town]', $address->town, ['class' => 'form-control', 'placeholder'=>'Type Town']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][zip]">Zip</label>
                    <div class="col-sm-8">
                        {!! Form::text('address['.$code.'][zip]', $address->zip, ['class' => 'form-control', 'placeholder'=>'Type Zip']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][phone]">Phone</label>
                    <div class="col-sm-8">
                        {!! Form::text('address['.$code.'][phone]', $address->phone, ['class' => 'form-control', 'placeholder'=>'Type Phone']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="address[{{$code}}][cell]">Cell</label>
                    <div class="col-sm-8">
                        {!! Form::text('address['.$code.'][cell]', $address->cell, ['class' => 'form-control', 'placeholder'=>'Type Cell']) !!}
                    </div>
                </div>
            @endforeach
        @else
    <h4>You don't have any address.</h4>
            @endif
{{--TODO Later--}}
        {{--<div class="col-sm-12 text-right">--}}
            {{--<a href="#">Add Address</a>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>




