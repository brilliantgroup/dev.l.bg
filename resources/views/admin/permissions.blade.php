@extends('layouts.admin')

@section('content')
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="table_permission">
            <thead>
            <tr>
                <th>ID</th>
                <th>Role</th>
                <th width="100">Action</th>
            </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
    </div>

    <script>


        $(document).ready(function () {
            dataTable = $('#table_permission').DataTable({
                "sPaginationType": "full_numbers",
                processing: true,
                serverSide: true,
                ajax: '{!! route('get.permissions') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'display_name', name: 'display_name'},
                    {"data": "action", className: "table-action"}
                ],
                columnDefs: [
                    {orderable: false, targets: [0, -1]}
                ],
                "drawCallback": function (settings) {
//                bindTable();
                    bind();
                },
                "aLengthMenu": [
                    [20, 40, 60, 100],
                    [20, 40, 60, 100],
                ]
            });
        });
    </script>
@endsection