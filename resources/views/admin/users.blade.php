@extends('layouts.admin')

@section('content')
    <div class="table-responsive">
        <table class="table table-hover table-striped" id="table_users">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th width="100">Action</th>
            </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
    </div>

    <script>


        $(document).ready(function () {
            dataTable = $('#table_users').DataTable({
                "sPaginationType": "full_numbers",
                processing: true,
                serverSide: true,
                ajax: '{!! route('get.users') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {"data": "action", className: "table-action"}
                ],
                columnDefs: [
                    {orderable: false, targets: [0, -1]}
                ],
                "drawCallback": function (settings) {
//                bindTable();
                    bind();
                },
                "aLengthMenu": [
                    [20, 40, 60, 100],
                    [20, 40, 60, 100],
                ]
            });
        });

        //      $(document).ready(function () {
        //         var table = $('#table_users').DataTable({
        //            "sPaginationType": "full_numbers",
        //            "iDisplayLength": 20,
        //            "processing": true,
        //            "serverSide": true,
        //            "oLanguage": { "sSearch": "" },
        //            "order": [
        //               [ 0, "asc" ]
        //            ],
        //            "ajax": {
        //               "url": "/__admin/modules/users/get.records.php",
        //               "type": "POST",
        //               "data": {
        //                  active:function() {
        //                     return $("#js_o_active").val();
        //                  },
        //                  usertype:function() {
        //                     return $("#js_o_usertype").val();
        //                  }
        //               }
        //            },
        //            "columns": [
        //               { "data": "name" },
        //               { "data": "role" },
        ////                { "data": "id" },
        //               { "data": "action", className: "table-action" }
        //            ],
        ////            columnDefs: [
        ////                { orderable: false, targets: [0, -1] }
        ////            ],
        //            "drawCallback": function (settings) {
        ////                bindTable();
        //               bind();
        //            },
        //            "aLengthMenu": [
        //               [20,40,60,100],
        //               [20,40,60,100],
        //            ]
        //         });

        //        bindTable();

        //         $('#js_o_active, #js_o_usertype').unbind().on('change', function () {
        //            table.ajax.reload();
        //         });

        //      });


//        $("#btn-save").on("click", function (e) {
//            $("#fa").on("click", "button.submit",function () {




    </script>


@endsection