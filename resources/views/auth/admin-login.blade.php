@extends('layouts.admin')

@section('content')
    <div class="signinpanel">

        <div class="row">

            <div class="col-md-7">

                <div class="signin-info">
                    <div class="logopanel">
                        <img src="/images/adminarea/logo_big_alis.png" title="Alis" width="200">
                    </div>
                    <!-- logopanel -->

                    <div class="mb20"></div>

                    <!--          <h5><strong></strong></h5>-->

                    <div class="mb20"></div>
                </div>
                <!-- signin0-info -->

            </div>
            <!-- col-sm-7 -->

            <div class="col-md-5">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                    {{ csrf_field() }}

                    <h4 class="nomargin">Sign In</h4>
                    <p class="mt5 mb20">Login to access your account.</p>

                    <input type="email" class="form-control uname" autofocus="" required="" placeholder="Email"
                           id="email" name="email"/>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif

                    <input type="password" class="form-control pword" required="" placeholder="Password" name="password"
                           id="password"/>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif

                    <p class="mt10 mb20">
                        <a href="{{ route('admin.password.request') }}">
                            <small>Forgot Your Password?</small>
                        </a>
                    </p>

                    {{--<div class="form-group">--}}
                        {{--<div class="col-md-6 col-md-offset-4">--}}
                            {{--<div class="checkbox">--}}
                                {{--<label>--}}
                                    {{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                                    {{--Remember Me--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <button class="btn btn-success btn-block">Sign In</button>

                </form>
            </div>
            <!-- col-sm-5 -->

        </div>
        <!-- row -->

        <div class="signup-footer">
            <div class="pull-left">&copy; Brilliant Group.&nbsp;{{date("Y")}}</div>
            <div class="pull-right">v: 7.0.0</div>
        </div>

    </div>

@endsection
