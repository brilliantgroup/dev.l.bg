@foreach($childs as $child)
    <tr>
        <td>
            @if ($child->page_level > '1')
                {{ str_repeat('&nbsp;&nbsp;', 4) }}<i class="fa fa-angle-right"></i>&nbsp;
            @endif
            {{ $child->page_name }}
        </td>
        <td class="table-action"><a class="update" alt="Update record" href="javascript:;" rel="{{ $child->id }}"><i
                        class="fa fa-pencil"></i></a>
            <a class="delete-row delete" alt="Remove record" href="javascript:;"
               rel="{{ $child->id }}"><i class="fa fa-trash-o"></i></a>
        </td>
    </tr>
    @if(count($child->childs))
        @include('manageChild',['childs' => $child->childs])
    @endif
@endforeach
