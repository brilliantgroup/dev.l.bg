<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Alis - @yield('title')</title>
    {{--Welcome to Admin Area!--}}

            <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/style.default.css") !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/alis.custom.css") !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/jquery.datatables.css") !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/jquery.gritter.css") !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/bootstrap-wysihtml5.css") !!}" rel="stylesheet">
    <link type="text/css" href="{!! asset("/css/bracket/css/wysiwyg-color.css") !!}" rel="stylesheet">

            <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?>
    </script>
    <script src="/js/jquery-1.10.2.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{!! asset("/js/html5shiv.js") !!}"></script>
    <
    script
    src = "{!! asset(" / js / respond.min.js
    ") !!}" ></script>
    <![endif]-->
</head>

<body class="{{ (Auth::guard('admin')->guest())?'signin':'stickyheader' }}">

<!-- Preloader -->
{{--<div id="preloader">--}}
    {{--<div id="status"><i class="fa fa-spinner fa-spin"></i></div>--}}
{{--</div>--}}

<section>
    <?
    //    if (!Auth::guest()) {
    if (!Auth::guard('admin')->guest()) {
    ?>
            <!-- leftpanel -->
    <div class="leftpanel sticky-leftpanel">
        <!-- end leftpanel -->

        <div class="logopanel">
            <img src="/images/adminarea/logo_small_alis.png">
        </div>
        <!-- logopanel -->

        <div class="leftpanelinner">

            @yield('top')
                    <!-- This is only visible to small devices -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media userlogged">
                    <div class="media-body">
                        {{--<h4>Hello {{Auth::user()->name}} !</h4>--}}
                        <h4>Hello {{Auth::guard('admin')->user()->name}} !</h4>
                        <span>"Life is so..."</span>
                    </div>
                </div>


                <h5 class="sidebartitle actitle">Account</h5>
                <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                    <li><a class="profile" rel="/admin/profile" href="javascript:;"><i
                                    class="fa fa-user"></i>
                            <span>Profile</span></a></li>
                    <li><a class="js-settings" href="javascript:;"><i class="fa fa-cog"></i>
                            <span>Settings</span></a></li>
                    <li><a href="javascript:logoff()"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
            <h5 class="sidebartitle">Navigation</h5>
            @include('partials.menu')

        </div>
        <!-- leftpanelinner -->
    </div>

    <div class="mainpanel">
        <!-- headerbar-->
        <div class="headerbar">
            @include('partials.headerbar')
        </div>
        <!-- end headerbar-->

        <!-- pageheaderbar -->
        <div class="pageheader">
            @include('partials.pageheader')

        </div>
        <!-- end pageheaderbar -->
        <!-- contentpanel -->
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div id="listcontent">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- panel -->

        </div>
        <!-- end contentpanel -->
    </div>
    <?
    } else {
    ?>
    @yield('content')
    <?
    }
    ?>
</section>

{{--<form name="fa" id="fa" method="post" action="#" enctype="multipart/form-data">--}}
    <div class="modal fade" id="myModal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">..</h4>
                </div>
                <div class="modal-body custom-bg"></div>
                <div class="modal-footer popup_button">

                </div>
            </div>
            <!-- modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
{{--</form>--}}
<!-- modal -->
@yield('bottom_sign')
        <!-- Scripts -->
{{--<script src="/js/app.js"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
{{--<script src="/js/jquery-1.10.2.min.js"></script>--}}
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.cookies.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/jquery.gritter.min.js"></script>
<script src="/js/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>

<script src="/js/modernizr.min.js"></script>
<script src="/js/retina.min.js"></script>

<script src="/js/wysihtml5-0.3.0.min.js"></script>
<script src="/js/bootstrap-wysihtml5.js"></script>

<script src="/js/custom.js"></script>
<script src="/js/alis.custom.js"></script>
</body>
</html>
