<a class="menutoggle"><i class="fa fa-bars"></i></a>

<div class="header-right">
    <ul class="headermenu">
        <li><div id="loading"><img src="/images/adminarea/loader.gif" alt=""></div></li>
        <li>
            <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    Hello {{Auth::guard('admin')->user()->name}}!
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li><a class="profile" rel="/admin/profile" href="javascript:;"><i
                                    class="glyphicon glyphicon-user"></i> My Profile</a>
                    </li>
                    {{--<li><a href="{{ route('admin.settings') }}"><i--}}
                                    {{--class="glyphicon glyphicon-cog"></i>Settings</a>--}}
                    {{--</li>--}}
                    <li>
                        <a href="{{ route('admin.logout') }}"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"><i
                                    class="glyphicon glyphicon-log-out"></i>
                            Log out
                        </a>

                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>