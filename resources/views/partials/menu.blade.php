@if (count($menu))
    <ul class="nav nav-pills nav-stacked nav-bracket" id="navmenu">
        @foreach($menu as $page)
            <li class="<?=(($page["is_children"]) ? 'nav-parent' : '')?> ">
                <a href="{{(($page["is_children"])?'#':route($page["page_route"]))}}"
                   title="{{$page["page_name"]}}"><i
                            class="{{$page["page_icon"]}}"></i><span>{{$page["page_name"]}}</span></a>
                @if (isset($page["childrens"]))
                    <ul class="children ">
                        @foreach($page["childrens"] as $childrens)
                            <li class="{{($currentPage->page_route==$childrens["page_route"])?'active':''}}"><a
                                        href="{{route($childrens["page_route"])}}"
                                        title="{{$childrens["page_name"]}}"><i
                                            class='fa fa-caret-right'></i>{{$childrens["page_name"]}}</a></li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
@else
    Menu is not available! :(
@endif
<style>
    #navmenu i {
        margin-bottom: 4px;
    }
</style>