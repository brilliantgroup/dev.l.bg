<h2>
    <i class="{{$currentPage->page_icon}}"></i>{{$currentPage->page_name}}
    <span>
      <ol class="breadcrumb">
          <li><a href="{{ route('admin.dashboard') }}">Home</a></li>
          @if (count($breadcrumbs))
              @foreach($breadcrumbs as $page)
                  <li><a href="{{route($page->page_route)}}"> {{$page->page_name}}</a></li>
              @endforeach
          @endif
      </ol>
</span>
</h2>
<div class="breadcrumb-wrapper">
    <ol class="breadcrumb">
        @if ($currentPage->page_modify && $pagePermissions->add)
            <li><a href="#" class="add_new_record" rel="/{{ Route::current()->uri }}/create">Add new record</a></li>
        @endif
        @if ($currentPage->page_refresh)
            <li><a href="#" class="refresh">Refresh</a></li>
        @endif
    </ol>
</div>