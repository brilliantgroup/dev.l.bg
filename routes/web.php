<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@userLogout')->name('user.logout');

//Route::group(['prefix' => 'admin',  'middleware' => 'auth:admin'], function() {
Route::prefix('admin')->group(function () {

    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    Route::post('/settings', 'Admin\AdminPagesController@index')->name('admin.settings');

    //Profile
//    Route::get('/profile/id/{id}', 'UserController@edit')->name('admin.user.edit');
    Route::get('/profile', 'AdminController@edit')->name('admin.profile');
    Route::put('/profile', 'AdminController@update');

    // Password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

    // Admin Pages
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/adminpages', 'Admin\AdminPageController@index')->name('admin.pages');

    // Users
    Route::get('/users', 'UserController@index')->name('admin.users');
    Route::post('/users/create', 'UserController@store');
    Route::put('/users/id/{id}', 'UserController@update');
    Route::get('/users/id/{id}', 'UserController@edit')->name('admin.user.edit');
    Route::delete('/users/{id}', 'UserController@destroy')->name('admin.users.delete');
    Route::get('/users/create', 'UserController@create')->name('admin.user.create');
    Route::get('getUsers', ['as' => 'get.users', 'uses' => 'UserController@getData']);

    // Admins
    Route::get('/admins', 'Admin\AdminController@index')->name('admin.admins');
    Route::post('/admins/create', 'Admin\AdminController@store');
    Route::put('/admins/id/{id}', 'Admin\AdminController@update');
    Route::get('/admins/id/{id}', 'Admin\AdminController@edit')->name('admin.admins.edit');
    Route::delete('/admins/{id}', 'Admin\AdminController@destroy')->name('admin.admins.delete');
    Route::get('/admins/create', 'Admin\AdminController@create')->name('admin.admins.create');
    Route::get('getAdmins', ['as' => 'get.admins', 'uses' => 'Admin\AdminController@getData']);

    // Admin Pages
    Route::get('/adminpages', 'Admin\AdminPageController@index')->name('admin.pages');
    Route::post('/adminpages/create', 'Admin\AdminPageController@store');
    Route::put('/adminpages/id/{id}', 'Admin\AdminPageController@update');
    Route::get('/adminpages/id/{id}', 'Admin\AdminPageController@edit')->name('admin.admin.pages.edit');
    Route::delete('/adminpages/{id}', 'Admin\AdminPageController@destroy')->name('admin.admin.pages.delete');
    Route::get('/adminpages/create', 'Admin\AdminPageController@create')->name('admin.admin.pages.create');
    Route::get('getAdminPages', ['as' => 'get.adminpages', 'uses' => 'Admin\AdminPageController@getData']);

    // Admin Pages Permission
    Route::get('/permissions', 'Admin\PermissionsController@index')->name('admin.permissions');
    Route::post('/permissions/create', 'Admin\PermissionsController@store');
    Route::put('/permissions/id/{id}', 'Admin\PermissionsController@update');
    Route::get('/permissions/id/{id}', 'Admin\PermissionsController@edit')->name('admin.admin.pages.edit');
    Route::delete('/permissions/{id}', 'Admin\PermissionsController@destroy')->name('admin.admin.pages.delete');
    Route::get('/permissions/create', 'Admin\PermissionsController@create')->name('admin.admin.pages.create');
    Route::get('getPermissions', ['as' => 'get.permissions', 'uses' => 'Admin\PermissionsController@getData']);


    // Admin Site Pages
    Route::get('/sitepages', 'Admin\AdminSitePageController@index')->name('admin.site.pages');
    Route::post('/sitepages/create', 'Admin\AdminSitePageController@store');
    Route::put('/sitepages/id/{id}', 'Admin\AdminSitePageController@update');
    Route::get('/sitepages/id/{id}', 'Admin\AdminSitePageController@edit')->name('admin.site.pages.edit');
    Route::delete('/sitepages/{id}', 'Admin\AdminSitePageController@destroy')->name('admin.site.pages.delete');
    Route::get('/sitepages/create', 'Admin\AdminSitePageController@create')->name('admin.site.pages.create');
    Route::get('getPages', ['as' => 'get.pages', 'uses' => 'Admin\AdminSitePageController@getData']);

//Admin Countries
    Route::get('/countries', 'Admin\CountriesController@index')->name('admin.countries');
    Route::post('/countries/create', 'Admin\CountriesController@store');
    Route::put('/countries/id/{id}', 'Admin\CountriesController@update');
    Route::get('/countries/id/{id}', 'Admin\CountriesController@edit')->name('admin.countries.edit');
    Route::delete('/countries/{id}', 'Admin\CountriesController@destroy')->name('admin.countries.delete');
    Route::get('/countries/create', 'Admin\CountriesController@create')->name('admin.countries.create');
    Route::get('getCountries', ['as' => 'get.countries', 'uses' => 'Admin\CountriesController@getData']);

//Admin Categories
    Route::get('/categories', 'Admin\CategoriesController@index')->name('admin.categories');
    Route::post('/categories/create', 'Admin\CategoriesController@store');
    Route::put('/categories/id/{id}', 'Admin\CategoriesController@update');
    Route::get('/categories/id/{id}', 'Admin\CategoriesController@edit')->name('admin.categories.edit');
    Route::delete('/categories/{id}', 'Admin\CategoriesController@destroy')->name('admin.categories.delete');
    Route::get('/categories/create', 'Admin\CategoriesController@create')->name('admin.categories.create');
    Route::get('getCategories', ['as' => 'get.categories', 'uses' => 'Admin\CategoriesController@getData']);

    //Admin Products
    Route::get('/products', 'Admin\ProductsController@index')->name('admin.products');
    Route::post('/products/create', 'Admin\ProductsController@store');
    Route::put('/products/id/{id}', 'Admin\ProductsController@update');
    Route::get('/products/id/{id}', 'Admin\ProductsController@edit')->name('admin.products.edit');
    Route::delete('/products/{id}', 'Admin\ProductsController@destroy')->name('admin.products.delete');
    Route::get('/products/create', 'Admin\ProductsController@create')->name('admin.products.create');
    Route::get('getProducts', ['as' => 'get.products', 'uses' => 'Admin\ProductsController@getData']);

//    Route::get('/pages/{route_url?}', 'Admin\ProductsController@index')->where('route_url','[contact|about]+' )->name('admin.pages');

});

